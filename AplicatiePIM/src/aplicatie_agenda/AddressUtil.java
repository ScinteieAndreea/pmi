package aplicatie_agenda;

public class AddressUtil {
	
	public static Address createAddress(){
		String option;
		System.out.println("Insert contact address:");
		System.out.println("What kind of address want toinsert?");
		System.out.println("1)American address");
		System.out.println("2)French address");
		option=AppMenu.getUserOption();
		
		System.out.println("Insert contact address country:");
		String country=AppMenu.getUserOption();
	    
		boolean matchCountry=country.matches("[a-zA-Z]+");
		if(!matchCountry)
		{
			System.out.println("Address country is not valid! It contains numbers. ");
		}
		
			
			
			System.out.println("Insert contact address city:");
		String city=AppMenu.getUserOption();
		
		boolean matchCity=city.matches("[a-zA-Z]+");
		if(!matchCity)
		{
			System.out.println("City is not valid! It contains numbers. ");
		}
		
		System.out.println("Insert contact address street:");
		String street=AppMenu.getUserOption();
		
		System.out.println("Insert contact address building number:");
		String buildingNumberString=AppMenu.getUserOption();
		int buildingNumber=0;
		
		try
		{
			buildingNumber=Integer.parseInt(buildingNumberString);
		}
		catch(NumberFormatException e)
		{
			System.out.println(e.getMessage());  //asa se ia mesajul dintr o exceptie aruncata
		}
		
		System.out.println("Insert contact address postal code:");
		String postalCodeString=AppMenu.getUserOption();
		int postalCode=	0;
		
		try
		{
			postalCode=Integer.parseInt(postalCodeString);
		}
		catch(NumberFormatException e)
		{
			System.out.println(e.getMessage());  //asa se ia mesajul dintr o exceptie aruncata
		}
		
		Address address=null;
		
		if(option.equalsIgnoreCase("1")) {
			
			System.out.println("Insert contact address zone:");
			String zone=AppMenu.getUserOption();
			
			address=new AmericanAddress(country, city, street, buildingNumber, postalCode, zone); //deoarece noi citim string trebuie sa convertim ce avem noi declarat ca int cu parse
			
		}
		else if(option.equalsIgnoreCase("2")) {
			
			System.out.println("Insert contact address district:");
			String districtString=AppMenu.getUserOption();
			int district=0;
			
			try
			{
				district=Integer.parseInt(districtString);
			}
			catch(NumberFormatException e)
			{
				System.out.println(e.getMessage());
			}
			
			address=new FrenchAddress(country, city, street,buildingNumber,postalCode,district);
		}
		else
		{
			 address=new Address(country,  city, street, buildingNumber, postalCode);
		}
		
		return address;
		
	}
	
	

}
