package aplicatie_agenda;

import java.time.LocalDate;
import java.util.Scanner;

public class AppMenu {
	
	
	 static Agenda agenda=new Agenda();  //pentru asta am creat in Agenda si un constructor default pentru a ne lasa sa apelam un constructor gol (default)

	public static void main(String[] args) {
		
		displayMenu();
	}

	private static void displayMenu() {
		displayMainMenu();

		String option = getUserOption();

		switch (option) {
			case "1":
				agenda.printContacts();
				ContactsUtil.handleContact();
				handleReturnToMM();
				break;

			case "2":
				agenda.printEvents();
				EventsUtil.handleEvents();
				handleReturnToMM();
				break;

			case "3":
				agenda.printTasks();
				TasksUtil.handleTasks();
				handleReturnToMM();
				
				break;

			case "4":
				agenda.printMeetings();
				MeetingUtil.handleMeeting();
				handleReturnToMM();
				
				break;

			case "5":
				System.exit(0);
				// break;

			default:
				System.out.println("Please type in a value between 1 and 5");
				break;
		}

	}
	

	
	public static void handleReturnToMM() {
		String option;
		System.out.println("Would you like to return to the main menu? Y/N");
		option = getUserOption();
		if (option.equalsIgnoreCase("Y")) {
			displayMenu();
		} else {
			System.out.println("Bye bye");
		}
	}
	
	public static String getUserOption() {
		Scanner sin = new Scanner(System.in);
		return sin.nextLine();
	}
	
	private static void displayMainMenu() {
		System.out.println("Please choose an option (ex: for option 1, press 1)");
		System.out.println("1) Contacts");
		System.out.println("2) Events");
		System.out.println("3) Tasks");
		System.out.println("4) Meetings");
		System.out.println("5) Exit");
		System.out.println("Please type your option here: ");
	}

}
