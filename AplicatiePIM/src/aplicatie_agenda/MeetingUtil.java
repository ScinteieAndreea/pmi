package aplicatie_agenda;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class MeetingUtil {
	
	private static void handleAddMeeting()
	{
		String option;
		System.out.println("Insert meeting name:");
		String name=AppMenu.getUserOption();
		System.out.println("Insert contact name:");
		String contactName=AppMenu.getUserOption();
		Contact contact=AppMenu.agenda.searchContact(contactName);
		
		if(contact==null)
		{
			System.out.println("Contact not found. Please insert contact!");
			
			contact=ContactsUtil.handleAddContact2();
		}
		
		
		
		System.out.println("Insert contact meeting date as: 2016-06-16");
		String meetingDateString=AppMenu.getUserOption();  
		
		System.out.println("Insert meeting address:");
		System.out.println("What kind of address want to insert?");
		System.out.println("1)American address");
		System.out.println("2)French address");
		option=AppMenu.getUserOption();
		
		System.out.println("Insert meeting address country:");
		String country=AppMenu.getUserOption();
		
		System.out.println("Insert meeting address city:");
		String city=AppMenu.getUserOption();
		
		System.out.println("Insert meeting address street:");
		String street=AppMenu.getUserOption();
		
		System.out.println("Insert meeting address building number:");
		String buildingNumberString=AppMenu.getUserOption();
		
       int buildingNumber=0;
		
		try
		{
			buildingNumber=Integer.parseInt(buildingNumberString);
		}
		catch(NumberFormatException e)
		{
			System.out.println(e.getMessage());  //asa se ia mesajul dintr o exceptie aruncata
		}
		
		System.out.println("Insert meeting address postal code:");
		String postalCodeString=AppMenu.getUserOption();
		int postalCode=	0;
		
		try
		{
			postalCode=Integer.parseInt(postalCodeString);
		}
		catch(NumberFormatException e)
		{
			System.out.println(e.getMessage());  //asa se ia mesajul dintr o exceptie aruncata
		}
		
		
		Address address=null;
		
		if(option.equalsIgnoreCase("1")) {
			
			System.out.println("Insert meeting address zone:");
			String zone=AppMenu.getUserOption();
			
			address=new AmericanAddress(country, city, street, buildingNumber, postalCode, zone); //deoarece noi citim string trebuie sa convertim ce avem noi declarat ca int cu parse
			
		}
		else if(option.equalsIgnoreCase("2")) {
			
			System.out.println("Insert contact address district:");
			String districtString=AppMenu.getUserOption();
			int district=0;
			
			try
			{
				district=Integer.parseInt(districtString);
			}
			catch(NumberFormatException e)
			{
				System.out.println(e.getMessage());
			}
			
			address=new FrenchAddress(country, city, street,buildingNumber,postalCode,district);
		}
		else
		{
			 address=new Address(country,  city, street, buildingNumber,postalCode);
		}
		
		
		LocalDate meetingDate=null;
		try {
			meetingDate=LocalDate.parse(meetingDateString) ;
		}
		
		catch(DateTimeParseException e)
		{
			System.out.println(e.getMessage());
		}
		
		
		Meeting meeting=new Meeting(name,contact,address, meetingDate);
	
		AppMenu.agenda.addMeeting(meeting);
		
		AppMenu.handleReturnToMM();
	}
	

	private static void handleRemoveMeeting()
	{
		
		System.out.println("Insert contact name to be deleted:");
		String name=AppMenu.getUserOption();
		
		AppMenu.agenda.removeMeeting(name);
		
		AppMenu.handleReturnToMM();
	}
	
	public static void handleMeeting()
	{
		String option;
		displayContactMenu();
		option=AppMenu.getUserOption();
		if(option.equalsIgnoreCase("1")) {
			handleAddMeeting();
			
		}
		if(option.equalsIgnoreCase("2")) {
			handleRemoveMeeting();
		}
		if(option.equalsIgnoreCase("3")) {
			System.exit(0);
		}
	}
	
	
	private static void displayContactMenu() {
		System.out.println("Please choose an option (ex: for option 1, press 1)");
		System.out.println("1) Add Meeting");
		System.out.println("2) Remove Meeting");
		System.out.println("3) Exit");
		System.out.println("Please type your option here: ");
	}
}

