package aplicatie_agenda;

public class AmericanPhoneNumber extends PhoneNumber {

	public final String PREFIX="1";

	public AmericanPhoneNumber(long phoneNumber) {
		super(phoneNumber);
		
	}

	@Override
	public String toString() {
		return "AmericanPhoneNumber [PREFIX=" + PREFIX + ", getPhoneNumber()=" + getPhoneNumber() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	
	
	
}
