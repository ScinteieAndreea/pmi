package aplicatie_agenda;

public class Owner extends Name{
	
	private Address address;

	public Owner(String name, Address address) {
		super(name);
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Owner [address=" + address + ", getName()=" + getName() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
	

}
