package aplicatie_agenda;

import java.time.LocalDate;

public class Meeting extends Name{
	
	private Contact contact;
	private Address address;
	private LocalDate date;
	public Meeting(String name, Contact contact, Address address, LocalDate date) {
		super(name);
		this.contact = contact;
		this.address = address;
		this.date = date;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Meeting [contact=" + contact + ", address=" + address + ", date=" + date + ", getName()=" + getName()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	
	

}
