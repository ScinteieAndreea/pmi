package aplicatie_agenda;

public class PhoneNumber {
	
	private long phoneNumber;

	public PhoneNumber(long phoneNumber) {
		super();
		if(phoneNumber>9999999999l) //aici trebuie pus de tipul long pentru ca default este int
		{
			throw new IllegalArgumentException("Numarul este invalid!");
		}
		this.phoneNumber = phoneNumber;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "PhoneNumber [phoneNumber=" + phoneNumber + "]";
	}
	
	

}
