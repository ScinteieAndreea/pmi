package aplicatie_agenda;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

        //am mai creat o clasa pentru metodele evenimentelor deoarece clasa principala AppMenu era prea mare
public class EventsUtil {

	public static void handleEvents()
	{
		String option;
		displayContactMenu();
		option=AppMenu.getUserOption();
		if(option.equalsIgnoreCase("1")) {
			handleAddEvent();
			
		}
		if(option.equalsIgnoreCase("2")) {
			handleRemoveEvent();
		}
		
		if(option.equalsIgnoreCase("3")) {
			System.exit(0);
		}
	}
	private static void displayContactMenu() {
		System.out.println("Please choose an option (ex: for option 1, press 1)");
		System.out.println("1) Add Event");
		System.out.println("2) Remove Event");
		System.out.println("3) Exit");
		System.out.println("Please type your option here: ");
	}
	
	private static void handleAddEvent()
	{
		String option;
		System.out.println("Insert event name:");
		String name=AppMenu.getUserOption();
		
		
		
		System.out.println("Insert contact event date as: 2016-06-16");
		String eventDateString=AppMenu.getUserOption();    
		
		LocalDate eventDate=null;
		try {
			eventDate=LocalDate.parse(eventDateString) ;
		}
		
		catch(DateTimeParseException e)
		{
			System.out.println(e.getMessage());
		}
		
		Address address=AddressUtil.createAddress();
		Event event=new Event(name,eventDate, address);
	
		AppMenu.agenda.addEvent(event);
		
		AppMenu.handleReturnToMM();
	}
	
	private static void handleRemoveEvent()
	{
		System.out.println("Insert contact name to be deleted:");
		String name=AppMenu.getUserOption();
		
		AppMenu.agenda.removeEvent(name);
		
		AppMenu.handleReturnToMM();
	}
}
