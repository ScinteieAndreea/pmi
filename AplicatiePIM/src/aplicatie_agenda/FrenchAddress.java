package aplicatie_agenda;

public class FrenchAddress extends Address{
	
	
	private int district;

	public FrenchAddress(String country, String city, String street, int buildingNumber, int postalCode, int district) {
		super(country, city, street, buildingNumber, postalCode);
		this.district = district;
	}

	public int getDistrict() {
		return district;
	}

	public void setDistrict(int zone) {
		this.district = district;
	}

	@Override
	public String toString() {
		return "FrenchAddress [district=" + district + ", getCountry()=" + getCountry() + ", getCity()=" + getCity()
				+ ", getStreet()=" + getStreet() + ", getBuildingNumber()=" + getBuildingNumber() + ", getPostalCode()="
				+ getPostalCode() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

	
	
	
	

}
