package aplicatie_agenda;

import java.time.LocalDate;

public class Event extends Name{
	
	private LocalDate date;
	private Address address;
	public Event(String name, LocalDate date, Address address) {
		super(name);
		this.date = date;
		this.address = address;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Event [date=" + date + ", address=" + address + ", getName()=" + getName() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	
	
	
	
	
	

}
