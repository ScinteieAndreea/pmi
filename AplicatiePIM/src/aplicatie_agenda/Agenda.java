package aplicatie_agenda;

import java.util.ArrayList;
import java.util.List;

public class Agenda {
	
	private List<Contact> contacts;
	private List<Meeting> meetings;
	private List<Task> tasks;
	private List<Event> events;
	private Owner ownerInformatin;
	
	
	public Agenda(List<Contact> contacts, List<Meeting> meetings, List<Task> tasks, List<Event> events,
			Owner ownerInformatin) {
		super();
		this.contacts = contacts;
		this.meetings = meetings;
		this.tasks = tasks;
		this.events = events;
		this.ownerInformatin = ownerInformatin;
	}
	
	
	
	
	public Agenda() {
		super();
	}




	public List<Contact> getContacts() {
		return contacts;
	}
	
	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public void addContact(Contact contact) //creem o metoda de adaugare si pentru a adauga cate un singur contact in lista nu cum setam mai sus toata lista
	{
		if(contacts==null)              //daca lista este null adaugarea arunca o exceptie => construim un if pentru a nu mai arunca exceptie
		{
			contacts=new ArrayList<>();  
		}
		this.contacts.add(contact);
	}
	
	public void removeContact(String name)
	{
		if(contacts==null)
		{
			return; //daca lista este goala nu mai sterge nimic si se intrerupe metoda
		}
//		for(Contact contact:contacts)                            // este acelasi lucru cu lambda expression de mai jos
//		{
//			if(contact.getName().contentEquals(name))
//			{
//				contacts.remove(contact);
//			}
//		}
		contacts.removeIf(contact->contact.getName().equalsIgnoreCase(name));   // in lambda expression nu mai putem folosi tot name
	}
	
	
	
	public List<Meeting> getMeetings() {
		return meetings;
	}
	
	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}
	
	public void addMeeting(Meeting meeting)   //creem o metoda de adaugare si pentru a adauga cate un singur meeting in lista nu cum setam mai sus toata lista
	{
		if(meetings==null)              //daca lista este null adaugarea arunca o exceptie => construim un if pentru a nu mai arunca exceptie
		{
			meetings=new ArrayList<>();  
		}
		this.meetings.add(meeting);
	}
	
	public void removeMeeting(String nume)
	{
		if(meetings==null)
		{
			return; //daca lista este goala nu mai sterge nimic si se intrerupe metoda
		}

		meetings.removeIf(meeting->meeting.getName().equalsIgnoreCase(nume));   // in lambda expression nu mai putem folosi tot name
	}
	
	
	public List<Task> getTasks() {
		return tasks;
	}
	
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	public void addTask(Task task)    //creem o metoda de adaugare si pentru a adauga cate un singur task in lista nu cum setam mai sus toata lista
	{
		if(tasks==null)              //daca lista este null adaugarea arunca o exceptie => construim un if pentru a nu mai arunca exceptie
		{
			tasks=new ArrayList<>();  
		}
		this.tasks.add(task);
	}
	
	public void removeTask(String name)
	{
		if(tasks==null)
		{
			return; //daca lista este goala nu mai sterge nimic si se intrerupe metoda
		}

		tasks.removeIf(task->task.getName().equalsIgnoreCase(name));   // in lambda expression nu mai putem folosi tot name
	}
	
	
	public Contact searchContact(String name)
	{
		if(contacts==null||contacts.isEmpty())
		{
			return null;
		}
		return contacts.stream().filter(contact->contact.getName().equalsIgnoreCase(name)).findFirst().get();   //findFirst intoarce un Optional si pentru a lua obiectul mai folosim metoda get() 
	}
	
	
	public List<Event> getEvents() {
		return events;
	}
	
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	public void addEvent(Event event)  //creem o metoda de adaugare si pentru a adauga cate un singur event in lista nu cum setam mai sus toata lista
	{
		if(events==null)              //daca lista este null adaugarea arunca o exceptie => construim un if pentru a nu mai arunca exceptie
		{
			events=new ArrayList<>();  
		}
		this.events.add(event);
	}
	
	
	
	public void removeEvent(String name)
	{
		if(events==null)
		{
			return; //daca lista este goala nu mai sterge nimic si se intrerupe metoda
		}

		events.removeIf(event->event.getName().equalsIgnoreCase(name));   // in lambda expression nu mai putem folosi tot name
	}
	
	
	public Owner getOwnerInformatin() {
		return ownerInformatin;
	}
	
	public void setOwnerInformatin(Owner ownerInformatin) {
		this.ownerInformatin = ownerInformatin;
	}


	@Override
	public String toString() {
		return "Agenda [contacts=" + contacts + ", meetings=" + meetings + ", tasks=" + tasks + ", events=" + events
				+ ", ownerInformatin=" + ownerInformatin + "]";
	}
	
	
	public void printContacts()
	{
		if(contacts!=null)
		{
		contacts.stream().forEach(contact->System.out.println(contact.toString()));
		}
		else 
			{
			System.out.println("Lista de contacte este goala!");
			}
	}
	
	public void printMeetings()
	{
		if(meetings!=null)
		{
		meetings.stream().forEach(meeting->System.out.println(meeting.toString()));
		}
		else 
			{
			System.out.println("Lista de meeting-uri este goala!");
			}
	}
	
	
	public void printEvents()
	{
		if(events!=null)
		{
		events.stream().forEach(event->System.out.println(event.toString()));
		}
		else 
			{
			System.out.println("Lista de event-uri este goala!");
			}
	}
	
	
	public void printTasks()
	{
		if(tasks!=null)
		{
		tasks.stream().forEach(task->System.out.println(task.toString()));
		}
		else 
			{
			System.out.println("Lista de task-uri este goala!");
			}
	}
	
	
	

}
