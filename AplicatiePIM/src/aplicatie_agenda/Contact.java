package aplicatie_agenda;

import java.time.LocalDate;

public class Contact extends Name{
	
	
	private String email;
	private LocalDate birthDate;  //este un obiect predefinit de java care retine data
	private Address address;
	private PhoneNumber phoneNumber;
	
	
	public Contact(String name, String email, LocalDate birthDate, Address address, PhoneNumber phoneNumber) {
		super(name);
		this.email = email;
		this.birthDate = birthDate;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public PhoneNumber getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(PhoneNumber phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	@Override
	public String toString() {
		return "Contact [email=" + email + ", birthDate=" + birthDate + ", address=" + address + ", phoneNumber="
				+ phoneNumber + ", getName()=" + getName() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	
	
	

}
