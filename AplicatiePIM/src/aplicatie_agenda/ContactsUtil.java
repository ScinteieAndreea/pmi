package aplicatie_agenda;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactsUtil {
	
	public static Contact handleAddContact2()   //creem o metoda de adaugare care returneaza o valoare pentru a o apela in clasa MeetingUtil pentru a putea adauga un nou contact in cazul in care cel cautat la Meeting este null
												//trebuie sa facem aceasta metoda care returneaza o valoare pentru ca atunci cand o apelam in MeetingUtil atunci cand vrem sa adaugam contactul care este null sa ne returneze valoarea contactului pentru ca el sa nu mai fie considerat de catre compilator null
												//daca am fi avut metoda care returna void chiar daca noi am fi introdus in contactul verificat o valoare, el ar fi ramas tot null pentru metoda nu ar fi returnat nimic
	{
		String option;
		System.out.println("Insert contact name:");
		String name=AppMenu.getUserOption();
		
		System.out.println("Insert contact email:");
		String email=AppMenu.getUserOption();
		String regex = "^(.+)@(.+)$";          //expresie regulata(limbaje formale)
		 
		Pattern pattern = Pattern.compile(regex);
		 
		Matcher matcher = pattern.matcher(email);    //vedem daca se potriveste cu patternul
		
		
		if(!matcher.matches())  //  matcher.matches()!=true daca nu se potriveste
		{
			System.out.println("Email address is not valid!");
		}
		
		
		System.out.println("Insert contact birthdate as: 2016-06-16");
		String birthDateString=AppMenu.getUserOption();    // bithDate este un obiect de tip LocalDate si trebuie sa cautam pe net cum facem sa convertim un String in LocalDate
		LocalDate birthDate=null;
		try {
			birthDate=LocalDate.parse(birthDateString) ;
		}
		
		catch(DateTimeParseException e)
		{
			System.out.println("The date is not valid!");
		}
		
		System.out.println("What type of phone number do you want to insert?");
		System.out.println("1)American ");
		System.out.println("2)French ");
		option=AppMenu.getUserOption();
		
		System.out.println("Insert contact address phone number:");
		String phoneNumber=AppMenu.getUserOption();
		
		PhoneNumber phonenumber=null;
		
		if(option.equalsIgnoreCase("1")) {
			
			try {
				phonenumber=new AmericanPhoneNumber(Long.parseLong(phoneNumber));
				
			}
			catch(IllegalArgumentException e){
				
				System.out.println(e.getMessage());
					
				}
			}
			
		
		else if(option.equalsIgnoreCase("2")) {
			
			
			try {
				phonenumber=new AmericanPhoneNumber(Long.parseLong(phoneNumber));
				
			}
			catch(IllegalArgumentException e){
				
				System.out.println(e.getMessage());
					
				}
			
		}
		
		
		
		
		Address address=AddressUtil.createAddress();
		Contact contact=new Contact(name, email, birthDate, address, phonenumber);
	
		AppMenu.agenda.addContact(contact);
		return contact;
	}
	
	public static void handleAddContact()
	{
		handleAddContact2();
		
		AppMenu.handleReturnToMM();
	}
	

	private static void handleRemoveContact()
	{
		
		System.out.println("Insert contact name to be deleted:");
		String name=AppMenu.getUserOption();
		
		AppMenu.agenda.removeContact(name);
		
		AppMenu.handleReturnToMM();
	}
	
	public static void handleContact()
	{
		String option;
		displayContactMenu();
		option=AppMenu.getUserOption();
		if(option.equalsIgnoreCase("1")) {
			handleAddContact();
			
		}
		if(option.equalsIgnoreCase("2")) {
			handleRemoveContact();
		}
		if(option.equalsIgnoreCase("3")) {
			System.exit(0);
		}
	}
	
	
	private static void displayContactMenu() {
		System.out.println("Please choose an option (ex: for option 1, press 1)");
		System.out.println("1) Add Contacts");
		System.out.println("2) Remove Contact");
		System.out.println("3) Exit");
		System.out.println("Please type your option here: ");
	}
}
