package aplicatie_agenda;

public class AmericanAddress extends Address{
	
	private String zone;

	public AmericanAddress(String country, String city, String street, int buildingNumber, int postalCode,
			String zone) {
		super(country, city, street, buildingNumber, postalCode);
		this.zone = zone;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	@Override
	public String toString() {
		return "AmericanAddress [zone=" + zone + ", getCountry()=" + getCountry() + ", getCity()=" + getCity()
				+ ", getStreet()=" + getStreet() + ", getBuildingNumber()=" + getBuildingNumber() + ", getPostalCode()="
				+ getPostalCode() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

	
	
	

}
