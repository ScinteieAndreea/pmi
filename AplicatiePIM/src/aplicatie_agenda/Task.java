package aplicatie_agenda;

import java.time.LocalDate;

public class Task extends Name {
	
	
	private String description;
	private LocalDate date;
	public Task(String name, String description, LocalDate date) {
		super(name);
		this.description = description;
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "Task [description=" + description + ", date=" + date + ", getName()=" + getName() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
