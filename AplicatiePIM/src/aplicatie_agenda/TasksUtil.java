package aplicatie_agenda;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class TasksUtil {

		public static void handleTasks()
		{
			String option;
			displayTaskMenu();
			option=AppMenu.getUserOption();
			if(option.equalsIgnoreCase("1")) {
				handleAddTask();
				
			}
			if(option.equalsIgnoreCase("2")) {
				handleRemoveTask();
			}
			if(option.equalsIgnoreCase("3")) {
				System.exit(0);
			}
		}
		private static void displayTaskMenu() {
			System.out.println("Please choose an option (ex: for option 1, press 1)");
			System.out.println("1) Add Task");
			System.out.println("2) Remove Task");
			System.out.println("3) Exit");
			System.out.println("Please type your option here: ");
		}
		
		private static void handleAddTask()
		{
			String option;
			System.out.println("Insert Task name:");
			String name=AppMenu.getUserOption();
			
			System.out.println("Insert Task description:");
			String description=AppMenu.getUserOption();
			
			
			
			System.out.println("Insert Task date as: 2016-06-16");
			String taskDateString=AppMenu.getUserOption();    
			LocalDate taskDate=null;
		try {
				 taskDate=LocalDate.parse(taskDateString);
		}
		catch(DateTimeParseException e)
		{
			System.out.println("Formatul datei este gresit");
		  
		}		
			
		Task task=new Task(name,description, taskDate);
		
			AppMenu.agenda.addTask(task);
			
			AppMenu.handleReturnToMM();
		}
		
		public static void handleRemoveTask()
		{
			System.out.println("Insert task name to be deleted:");
			String name=AppMenu.getUserOption();
			
			AppMenu.agenda.removeTask(name);
			
			AppMenu.handleReturnToMM();
		}
	}

